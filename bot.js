console.log("STARTING");
console.log("--------------------------------------------------");
const axios = require("axios");

const maxColor = 16777215;
const dayms = 86400000 - (3654321 * 2);
const minutems = 60000;

var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');
const { createHash } = require('crypto');

console.log("Requires loaded in.");

var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

console.log("initializing...");

var initialized = false;

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
    {
        if (err)
        {
            console.log(err);
            bot.sendMessage({
                to: bot.data.lastServer,
                message: "Failed to create a back-up.",
                typing: false
            });
        }
        else
        {
            if (!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
};

bot.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

console.log("savedata function defined");

bot.on('ready', function (evt)
{
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');

    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch (e)
        {
            if (bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if (bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch (e)
                {
                    if (bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if (bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || {};

        console.log("Checking if I need to send update message...");

        if (bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Updated!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notify to do");
        }
    });

    setTimeout(() => 
    {
        crashcrash();
    }, minutems * 15);

    setTimeout(() =>
    {
        let posted = false;
        Object.entries(bot.data.servers).forEach(entry => 
        {
            if (posted)
                return;

            const [id, server] = entry;
            console.log("Start voor server: " + bot.data.servers[id].server);
            if (server.channel)
            {
                console.log("Kanaal gevonden: " + server.channel);
                if (!server.time || server.time + dayms < Date.now())
                {
                    console.log("tijd om te posten");
                    server.time = Date.now();

                    let buffer = getBuffer("https://projects.timfalken.com/theevraagjes/", function ()
                    {
                        console.log("Buffer gereed");
                        console.log(buffer);

                        bot.uploadFile({
                            to: server.channel,
                            file: "./theevraagje.png",
                            filename: "theevraagje.png",
                            message: "",
                            typing: false
                        });
                    });

                    posted = true;
                    return;
                }
            }
        });

        bot.saveData();
    }, minutems);

});

console.log("onready defined");

bot.on('message', function (user, userID, channelID, message, evt)
{
    console.log(message);

    if (!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    if (!bot.data.servers[id])
    {
        bot.data.servers[id] = { mentions: {} };
    }

    var group = [userID];

    var words = message.split(' ');

    if (Date.now() > bot.lastSave + 3600000) // one hour
    {
        bot.saveBackup(true);
        bot.lastSave = Date.now();
    }

    bot.data.servers[id].server = id;

    if (message.substring(0, 1) == '^' && userID == "189716214795337729" || userID == "124136556578603009") 
    {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        var info = { user: user, userID: userID, channelID: channelID, message: message, evt: evt, serverId: id };

        if (cmd.toLowerCase() == "opdatum")
        {
            crash();
        }

        if (cmd.toLowerCase() == "thee")
        {
            bot.data.servers[id].channel = channelID;
            bot.sendMessage({
                to: bot.data.servers[id].channel,
                message: "OK",
                typing: false
            });
        }

        if (cmd.toLowerCase() == "vergeet")
        {
            bot.data.servers[id].mentions = {};

            bot.sendMessage({
                to: channelID,
                message: "Vergeten.",
                typing: false
            });
        }

        if (cmd.toLowerCase() == "toon")
        {
            bot.sendMessage({
                to: channelID,
                message: "```\n" + JSON.stringify(bot.data.servers[id]) + "\n```",
                typing: false
            });
        }
    }

    bot.saveData();
});

async function getBuffer (url, callback)
{
    const response = await axios({
        method: "get",
        url: "https://projects.timfalken.com/theevraagjes/",
        responseType: "stream",
    });

    await response.data.pipe(fs.createWriteStream("./theevraagje.png"));

    setTimeout(
        function ()
        {
            callback && callback();
        }, 5000
    );
}

console.log("onmessage defined");
